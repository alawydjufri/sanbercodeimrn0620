var nama;
var peran;

nama = "";
peran = "";

if (!nama + !peran) {
    console.log("// Output untuk Input nama = '' dan peran = ''");
    console.log("Nama Harus diisi!");
}

nama = "Alawy";
peran = "";

if (nama = "Alawy" + !peran) {
    console.log("//Output untuk Input nama = 'John' dan peran = ''");
    
    console.log("Halo Alawy, Pilih peranmu untuk memulai game!");
}

nama = "Alawy";
peran = "Penyihir";

if (nama + peran) {
    console.log("//Output untuk Input nama = 'Alawy' dan peran 'Penyihir'");
    
    console.log("Selamat datang di Dunia Werewolf, Alawy");
    console.log("Halo Penyihir Alawy, kamu dapat melihat siapa yang menjadi werewolf!");
    
}

nama = "Sigit";
peran = "Guard";

if (nama + peran) {
    console.log("//Output untuk Input nama = 'Sigit' dan peran 'Guard'");
    
    console.log("Selamat datang di Dunia Werewolf, Selamat datang di Dunia Werewolf, Sigit");
    console.log("Halo Guard Sigit, kamu akan membantu melindungi temanmu dari serangan werewolf.");
    
}

nama = "Depri";
peran = "Werewolf";

if (nama + peran) {
    console.log("//Output untuk Input nama = 'Depri' dan peran 'Warewolf'");
    
    console.log("Selamat datang di Dunia Werewolf, Selamat datang di Dunia Werewolf, Depri");
    console.log("Halo Warewolf Depri, kamu akan memakan mangsa setiap malam!.");
    
}