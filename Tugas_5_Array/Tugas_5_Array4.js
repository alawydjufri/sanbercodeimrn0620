var input = [
    ["Nomor ID: 0001", "Nama Lengkap: Roman Alamsyah", "TTL: Bandar Lampung 21/05/1989", "Hobi: Membaca"],
    ["Nomor ID: 0002", "Nama Lengkap: Dika Sembiring", "TTL: Medan 10/10/1992", "Hobi: Bermain Gitar"],
    ["Nomor ID: 0003", "Nama Lengkap: Winona", "TTL: Ambon 25/12/1965", "Hobi: Memasak"],
    ["Nomor ID: 0004", "Nama Lengkap: Bintang Senjaya", "TTL: Martapura 6/4/1970", "Hobi: Berkebun"]
]


function dataHandling(params) {
    var output = '';

        for (let index = 0; index < input.length; index++) {
            output += input[index][0]+"\n";
            output += input[index][1]+"\n";
            output += input[index][2]+"\n";
            output += input[index][3]+"\n";
            output += input[index][4]+"\n\n";
        }

        
        return output;   
}

console.log(dataHandling(input));

