function sum(startNum, finishNum, step) {
    if (!step) {
        var total = 0;
        var ans = [];
        for (let index = startNum; index <= finishNum; index++) {
            ans.push(index);
        }

        for (var i in ans) {
            total = total+ans[i];
        }

        return total;
    } else {
        if (startNum<finishNum) {
            var ans = [];
            var total = 0;
            for (let index = startNum; step > 0 ?  index < finishNum : index > finishNum ; index += step) {
                ans.push(index);
            }
            for (var i in ans) {
               total = total+ans[i]         
           }
           return total;
           } else {
            var ans = [];
            var total = 0;
            for (let index = startNum; step > 0 ?  index >= finishNum : index <= finishNum ; index -= step) {
                ans.push(index);
            }
            for (var i in ans) {
                total = total+ans[i]         
            }
            return total;
        }
    }
}

 console.log(sum(5, 50, 2));
 console.log(sum(1,10)); // 55
 console.log(sum(20,10,2));
 console.log(sum(15,10));
 console.log(sum(1)) // 1
console.log(sum()) // 0

 

 