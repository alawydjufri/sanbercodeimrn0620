function rangeWithStep(startNum, finishNum, step) {
   if (startNum<finishNum) {
    var ans = [];
    for (let index = startNum; step > 0 ?  index < finishNum : index > finishNum ; index += step) {
        ans.push(index);
    }
    return ans;
   } else {
    var ans = [];
    for (let index = startNum; step > 0 ?  index >= finishNum : index <= finishNum ; index -= step) {
        ans.push(index);
    }
    return ans;
   }
}
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5,2, 1));
console.log(rangeWithStep(29, 2, 4));


