//Looping Pertama
var angka = 2;
var no;
console.log("LOOPING PERTAMA");

while(angka<=20){
    if (angka%2==0) {
        console.log(angka+"- I love coding"); 
    }
    angka++;
}

//Looping Kedua
var angka = 20;
var no
console.log('LOOPING KEDUA');

while(angka>=2){
    if (angka%2==0) {
        console.log(angka+"- I will become a mobile developer"); 
    }
    angka--;
}