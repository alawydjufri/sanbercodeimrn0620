var height = 8;
 var width = 8;
 var board = "";

 for (var y = 0; y < height; y++) {
     if(y>0) board += "\n";
     for (var x = 0; x < width; x++) {
         if ((x + y) % 2 == 0){
             board += " ";
         } else {
             board += "#";
         }
     }
 }

 console.log(board);